public class Libro{
    private Integer isbn;               //código de 13 dijitos.
    private String titulo;
    private String autor;
    private Integer numeroPaginas;

        //Setters

    public void setIsbn(Integer isbn){
        this.isbn=isbn;
    }
    public void setTitulo(String titulo){
        this.titulo= titulo;
    }
    public void setAutor(String autor){
        this.autor=autor;
    }
    public void setNumeroPaginas(Integer numeroPaginas){
        this.numeroPaginas=numeroPaginas;
    }

        //getters

    public Integer getIsbn(){
        return isbn;
    }
    public String getTitulo(){
        return titulo;
    }
    public String getAutor(){
        return autor;
    }
    public Integer getNumeroPaginas(){
        return numeroPaginas;
    }

        //método para mostrar la información relativa al libro.

    public void mostrarInformacion(){
        System.out.println("El libro con ISBN "+ isbn + ", creado por el autor: "+ autor + " tiene " + numeroPaginas +" paginas.");
    }
}