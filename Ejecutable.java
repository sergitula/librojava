public class Ejecutable {
    public static void main(String [] args){
        Libro libro1 = new Libro();
        Libro libro2 = new Libro();

        //mostarlos por pantalla

        libro1.setIsbn(1234567890);
        libro1.setTitulo("Código Limpio");
        libro1.setAutor("Robert C.Martin");
        libro1.setNumeroPaginas(453);

        libro2.setIsbn(908765431);
        libro2.setAutor("Proga");
        libro2.setTitulo("Probandofdasdf");
        libro2.setNumeroPaginas(20);

        libro1.mostrarInformacion();
        libro2.mostrarInformacion();
    }
}
